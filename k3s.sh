#!/bin/bash
sudo yum install -y container-selinux selinux-policy-base
sudo rpm -i https://rpm.rancher.io/k3s-selinux-0.1.1-rc1.el7.noarch.rpm
sudo yum install python3 -y
pip3 install requests --user
curl -sfL https://get.k3s.io | sh -
sudo chmod -R 755 /etc/rancher/
k3s kubectl create deployment nginx --image=webdevops/php-nginx:7.1-alpine
k3s kubectl expose deployment/nginx --port=80 --target-port=80 --name=nginx-service --type=LoadBalancer
sleep 120
python3 test.py
