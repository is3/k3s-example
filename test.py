import requests
import re
import subprocess


def get_url():
    command = "k3s kubectl get services"
    services = subprocess.check_output(command, shell=True)
    services = services.decode("utf-8")
    lb = re.search("(LoadBalancer\s*)(\d*\.\d*.\d*.\d*)", services)
    service_ip = lb[2]
    url = f"http://{service_ip}"
    return url

def get_status(url):
    r = requests.get(url)
    if r.status_code == 403:
        print("Service started correctly")
    else:
        print("Service error check logs")

if __name__ == "__main__":
    check_url = get_url()
    get_status(check_url)
